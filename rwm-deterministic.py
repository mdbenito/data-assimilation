#############################################################################
# Random Walk Metropolis for deterministic dynamics (§3.2.3 in the book)
#
#       v_{j+1} = Ψ(v_j), j = 0, 1, ..., J-1
#       y_{j+1} = h(v_{j+1}) + η
#
# where Ψ is a rotation in the plane.
#
# NOTES:
#  * If we have a large number of observations (J large), the computation of
#    the negative log posterior involves the multiplication of long vectors.
#    This leads to errors and bad performance, so I split it using a thread
#    pool. This is however unnecessary for J < ~ 1e5 which is anyway very
#    costly. In order to have reasonable running times J has to be of order
#    1e2 or 1e3.
#  * Incidentally, this shows how using the smoothing posterior is actually
#    unpractical for any decent number of observations.
#  * The effectiveness of the RWM varies wildly depending on the initial
#    condition, the parameter beta and the variance of the noise (!!)
#  * Globals are all over the place... :(
#
#############################################################################
from concurrent.futures import ThreadPoolExecutor
import numpy as np
import matplotlib.pyplot as pl
from utils import mult, iterate, available_cpu_count


def mult_chunk(vec):
    # print('chunk shape={}'.format(vec.shape))
    return np.sum(mult(vec, GammaInv, vec.T))


# FIXME!! Using globals! (m0, C0Inv, Psi, ...)
def nlogpost(v0: np.ndarray, y: np.ndarray) -> np.float64:
    """
    Unnormalized negative log posterior distribution of the initial signal
    given the data log p(v0|y). Following Theorem 2.11 the density is
    proportional to

        exp(-Idet(v0;y)),

    where

        Idet(v0;y) = Jdet(v0) + Phidet(v0;y)

    Notice that we must recompute the sequence h(Ψ_j+1(v0)) for every choice
    of v0.

    :param v0: data point (array n x 1)
    #:param y: observations (array J x n)
    :return: Idet(v0; y)
    """
    dif = v0 - m0
    Jdet = 0.5 * mult(dif, C0Inv, dif.T)

    J = y.shape[0] + 1
    h = np.array(list(iterate(Psi, v0, J)))[1:]
    # FIXME: for some reason numpy can't multiply (y-h)^T*GammaInv*(y-h)
    # when the array is of size ~ (1e5, 2) To fix this we split the
    # computation in chunks (and across threads as a bonus)
    MAXSIZE = 1e3
    workers = available_cpu_count()  # TODO: use this to adjust num_chunks
    num_chunks = np.ceil(J/MAXSIZE)
    Phidet = 0.
    with ThreadPoolExecutor(max_workers=workers) as ex:
        for r in ex.map(mult_chunk, np.array_split(y - h, num_chunks)):
            Phidet += r
        Phidet *= 0.5
    # print('Done computing Phidet={}'.format(Phidet))
    return Jdet + Phidet


def a(Idet_current, Idet_proposal):
    """
    Acceptance probability of a proposal

    The acceptance probability is simplified for RWM thanks to the symmetry of
    the transition kernel

        q(u,w) = N(w|u,β*Cprop) = N(u|w,β*Cprop) = q(w,u)

    Therefore:

        a(u,w) = min {1, ρ(w)/ρ(u)}

    and because ρ is proportional to exp(-Idet), Idet being the negative log
    posterior, we can compute

        ρ(w)/ρ(u) = exp (Idet_current - Idet_proposal)

    and this is only < 1 if Idet_proposal > Idet_current

    :param Idet_current:
    :param Idet_proposal:
    :return:
    """
    return min(1., np.exp(Idet_current - Idet_proposal))


if __name__ == '__main__':
    # Our dynamics is linear and given by a rotation:
    phi = 0.1                   # Angle of rotation
    M = np.array([[np.cos(phi), -np.sin(phi)],
                  [np.sin(phi), np.cos(phi)]])


    def Psi(v: np.ndarray) -> np.float64:
        return np.dot(M, v)

    N = int(1e4)                # Number of steps in the Markov chain
    J = 100                     # Number of samples after v0
    n = M.shape[1]              # Dimension of each signal sample
    m0 = np.zeros(n)            # Mean of the initial distribution of v0
    C0 = 3 * np.identity(n)     # Covariance of the initial distribution of v0
    Gamma = 0.5 * np.identity(n)  # Covariance of the observation noise η
    beta = 0.1                  # Scaling parameter for the proposals
    Cprop = np.identity(n)      # Covariance for the RWM proposals

    print('Random Walk Montecarlo, N = {0} steps, beta = {1}'.format(N, beta))
    print('Pre-computing stuff...')
    #  Pre-compute some matrices and the gaussian draws for the proposals
    GammaInv = np.linalg.inv(Gamma)
    C0Inv = np.linalg.inv(C0)
    ww = np.random.multivariate_normal(np.zeros(n), beta * Cprop, N)

    print('Simulating dynamics and observations...')
    v0 = np.random.multivariate_normal(m0, C0)
    v = np.array(list(iterate(Psi, v0, J)))
    # The observation operator is the identity
    h = v[1:]
    # The observation noise is i.i.d.
    y = v[1:] + np.random.multivariate_normal(np.zeros(n), Gamma, v.shape[0]-1)

    print('Running... ', end='', flush=True)
    ####
    #  Sample the Markov chain
    u = np.empty((N, n))
    # If u[0] ~ ρ, the invariant measure for q, then u[j] ~ q ∀j
    # It seems sensible to pick the mean of the initial dist. (?)
    # However, since we have v[0] we can plug it in to reduce burn-in time
    # (cf. page...)
    u[0] = v0
    acceptance_rate = np.zeros(N)
    accepts = 0
    nlp = lambda w: nlogpost(w, y)  # Use the computed observations
    nlp_cur = nlp(u[0])       # Negative Log Posterior (Idet) of current state
    for k in range(1, N):
        ww[k] += u[k - 1]     # Recenter proposal at last value
        nlp_pro = nlp(ww[k])  # Negative Log Posterior (Idet) of proposal

        if np.random.uniform() <= a(nlp_cur, nlp_pro):
            u[k] = ww[k]    # Accept
            nlp_cur = nlp_pro
            accepts += 1
        else:
            u[k] = u[k - 1]   # Reject
        acceptance_rate[k] = accepts / N
        if k % int(np.ceil(J/50)) is 0:
            print('\rRunning... {0:3}% done'.format(int(np.ceil(100*k / N))),
                  end='', flush=True)

    print('\nmin u = {}, max u = {}'.format(u.min(axis=0), u.max(axis=0)))
    print('min y = {}, max y = {}'.format(y.min(axis=0), y.max(axis=0)))
    print("Average rate of acceptance = {:.3}".
          format(np.average(acceptance_rate)))

    ####
    # Plots:

    # Observations, signal and initial condition and steps of the RWM
    pl.subplot(3, 1, 1)
    pl.plot(y[:, 0], y[:, 1], 'gx')
    pl.plot(v[:-1, 0], v[:-1, 1], 'b-')
    pl.plot([v[0, 0]], [v[0, 1]], 'bo')
    pl.plot(u[::100, 0], u[::100, 1], 'r-')

    # Histogram of the RW. TODO: adjust the range to match the other plots
    pl.subplot(3, 1, 2)
    pl.hist2d(u[:, 0], u[:, 1], bins=20, normed=True)

    # Acceptance rate (roughly downsampled and averaged)
    pl.subplot(3, 1, 3)
    step = int(N / 100)
    a = acceptance_rate.reshape((int(N/step), step)).sum(axis=1) / step
    pl.plot(np.arange(N, step=step), a, 'g-')
    pl.show()
