# Simulation of two stochastic dynamics

import numpy as np
import matplotlib.pyplot as pl
from typing import Callable

# Ugly but convenient globals:

J = int(1e5)   # number of steps
Sigma = 0.25   # standard deviation for the Gaussian noise
m0 = 0         # mean for the initial distribution of the signal
C0 = 2         # variance for the initial distribution of the signal


def ExponentialDynamics(v: np.float64) -> np.float64:
    coeff = 0.95   # Ψ(v) = coeff * v
    return coeff * v


def NonlinearDynamics(v: np.float64) -> np.float64:
    alpha = 2.5
    return alpha * np.sin(v)


def simulate_stochastic(Psi: Callable[[np.float64], np.float64]) -> np.ndarray:
    """
    Computes one dimensional linear dynamics
        v_{j+1} = Ψ(v_j) + ξ
    :param Psi:
    :return:
    """
    v = np.zeros(J)
    v[0] = np.random.normal(m0, C0)
    if Sigma > 0:
        v[1:] = np.random.normal(0, Sigma, J-1)  # Pre-build all the noise (20% faster)
    for j in range(1, J):
        v[j] += Psi(v[j-1])
    return v


if __name__ == '__main__':
    # from timeit import Timer
    # t = Timer(lambda: simulate_ex1(v))
    # print(t.timeit(10000))
    # t = Timer(lambda: simulate_ex2(v))
    # print(t.timeit(10000))

    v = simulate_stochastic(NonlinearDynamics)

    pl.subplot(2, 1, 1)
    pl.plot(range(J), v, 'g-')

    #Sigma = 0
    # Downsample the deterministic signal to have a readable plot (but careful because it has period 2 !)
    # (This changes the period and actually makes the plot meaningless...)
    #pl.plot(np.arange(J)[1::9*81], simulate_stochastic(NonlinearDynamics)[1::9*81], 'b-')

    pl.subplot(2, 1, 2)
    # This code achieves almost the same as the line below...
    # h = np.histogram(v, bins=np.arange(-4, 4, 0.1), density=True)
    # pl.plot(h[1][:-1], h[0], 'r-')
    pl.hist(v, bins=np.arange(-4, 4, 0.1), normed=True, histtype='step')
    pl.show()
