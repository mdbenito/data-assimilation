# Data assimilation examples

These are implementations of some algorithms and examples from Law & Stuart's book:
[Data Assimilation](http://www.springer.com/us/book/9783319203249).

## Dependencies

* Python 3.5, Numpy, Matplotlib.

The easiest is to install everything using [conda](http://conda.pydata.org/).

## Why Python3?

Because we can! Also, thanks to (Ana)conda, installing any number of versions
of Python is almost trivial.

## License

Whatever...

