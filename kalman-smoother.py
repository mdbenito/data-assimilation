# coding=utf-8
# An implementation of a Kalman smoother, as presented in
# Andrew Stuart's "Data Assimilation", Chapter 3.
# By Philipp Wacker and Miguel de Benito Delgado
#
# We compute the posterior smoothing distribution p(v|y)
# given by Theorem 3.1

# Our signal v is given by linear stochastic dynamics:
#    v_{j+1} = Ψ(v_j) + ξ = M.v_j + ξ,
#    v_0 ~ N(m0, C0)
# where Ψ is the flow of a rotation around the origin by an angle φ
# and ξ ~ N(0, Σ) is gaussian noise. The observations are given by:
#    y_{j+1} = h(v_{j+1}) + η = H.v_{j+1} + η,
# where h is the observation operator and η ~ N(0, Γ)


import numpy as np
from numpy.linalg import inv
import matplotlib.pyplot as pl
from utils import mult

J = 300             # Number of observations / samples

phi = 0.1           # Angle of rotation
M = np.array([[np.cos(phi), -np.sin(phi)], [np.sin(phi), np.cos(phi)]])

# Uncomment this for a 3D example:
#M = np.array([[np.cos(phi), -np.sin(phi), 0],
#              [np.sin(phi), np.cos(phi), 0],
#              [0, 0, 1]])

n = M.shape[1]       # Dimension of each signal sample
H = np.identity(n)  # Observation operator

Sigma = .1 * np.identity(n)  # Covariance of the signal noise ξ
Gamma =  5 * np.identity(n)  # Covariance of the observation noise η

m0 = np.zeros(n)    # Mean of the initial distribution of v0
C0 = 4 * np.identity(n)  # Covariance of the initial distribution of v0

#### Simulate the signal and observations
#v = np.empty((n, J), dtype=np.float64)
#y = np.empty((n, J+1), dtype=np.float64)

v0 = np.random.multivariate_normal(m0, C0) # Initial value for the signal 
# First compute all the noise
v = np.random.multivariate_normal(np.zeros(n), Sigma, J+1)
y = np.random.multivariate_normal(np.zeros(n), Gamma, J)

v[-1] = v0
#y[0] += np.dot(H, v[0])
for j in range(0, J):
    v[j] += np.dot(M, v[j-1])
    y[j] += np.dot(H, v[j])


#### Smoothing of the signal

# Compute L, the precision matrix of the posterior distribution
C0Inv = inv(C0)
SInv = inv(Sigma)
GInv = inv(Gamma)
L = np.zeros((n*(J+1), n*(J+1)))
L[0:n, 0:n] = C0Inv + mult(M.T, SInv, M)
L[n*J : , n*J : ] = mult(H.T, GInv, H) + SInv

# Block diagonal
diag = mult(H.T, GInv, H) + mult(M.T, SInv, M) + SInv
for j in range(1, J):
    L[j*n : (j+1)*n, j*n : (j+1)*n] = diag

# Upper and lower block diagonals
udiag = np.dot(-M.T, SInv)
ldiag = np.dot(-SInv, M)
for j in range(0, J):
    L[j*n : (j+1)*n, (j+1)*n : (j+2)*n] = udiag
    L[(j+1)*n : (j+2)*n, j*n : (j+1)*n] = ldiag

# Compute m, the mean of the posterior distribution
r = np.zeros(n*(J+1))
r[0:n] = np.dot(C0Inv, m0)
tmp = mult(H.T, GInv)
for j in range(1, J+1):
    r[j*n : (j+1)*n] = np.dot(tmp, y[j-1])

LInv = inv(L)
#m = L\r;
m = np.dot(LInv, r)
m = m.reshape(J+1, n);

#### Plots

pl.subplot(2, 1, 1)
# Signal and observations
pl.plot(v[:-1, 0], v[:-1, 1], 'b-')
pl.plot(v[0, 0], v[0, 1], 'bo')
pl.plot(y[:, 0], y[:, 1], 'rx')

# Mean of the posterior distribution
pl.plot(m[:, 0], m[:, 1], 'g-')
pl.plot(m[0, 0], m[0, 1], 'go')

# Error (FIXME: why the shift?)
err = np.sqrt(np.square(v[:-1] - m[1:]).sum(axis=1))
pl.subplot(2, 1, 2)
pl.plot(err)


pl.show()
