# Metropolis-Hastings: sampling from a measure given by a known density
import numpy as np
import scipy.stats as ss
import matplotlib.pyplot as pl


def rho(x: np.float64) -> np.float64:
    """
    This is the density we want to sample from using Metropolis Hastings:
    It's the density of a uniform(-2, 1) + normal(0, 2)

    :param x: real number
    :return:
    """
    r = 1./3. if -2 <= x <= 1 else 0.
    r += ss.norm.pdf(x, 0, 2)
    return r / 2


def test_rho():
    x = np.arange(-10, 10, 0.1)
    y = np.array([rho(i) for i in x])
    pl.plot(x, y, 'r-')
    print("Integral = {}".format(np.trapz(y, x)))
    pl.show()


def q(u: np.float64):
    """
    A Markov transition kernel for a chain Q
       ∫q(u,w)dw = 1 for every u.
    The chain must be irreducible (all states communicate) and positive recurrent
    (the expected return time for any state is finite)
    :param u:
    :return: a scipy.stats._continuous_distns object
    """
    # return ss.norm(0, 2)  # Independent Metropolis Hastings (parallelizable!)
    # return ss.norm(u, 1)  # Random Walk Metropolis sampling

    return ss.uniform(u - 10., 20.)
    # NOTE: using ss.uniform(u - 10., u + 10) results in a completely wrong answer. Why?
    # The second parameter is the offset, so that the interval would be [u-10, 2u],
    # which is empty for u < -10. This means that the states u < -20 are all unreachable
    # for the chain Q but more importantly that each state u ∈ [-20,-10] communicates only
    # with itself (is absorbing), hence Q is not irreducible (a.k.a. ergodic in some contexts)


def a(u, w):
    return min(1., (rho(w)*q(w).pdf(u) / (rho(u)*q(u).pdf(w))))


J = int(1e4)
n = 0
u = np.empty(J)
u[0] = 0.  # u[0] ~ ρ
for j in range(1, J):
    w = q(u[j-1]).rvs()  # Proposal
    u[j] = w if np.random.uniform() <= a(u[j-1], w) else u[j-1]  # Accept / reject

pl.hist(u, bins=100, range=(-10, 10), normed=True, histtype='step')
test_rho()
pl.show()
